var ME405_Lab0x03_PC_RHall_8py =
[
    [ "sendChar", "ME405-Lab0x03-PC-RHall_8py.html#aeccd3fc3a0eb8704373ab14154bc3a9f", null ],
    [ "dat", "ME405-Lab0x03-PC-RHall_8py.html#af569154b4842f0ee2bfe2e8d55b1372a", null ],
    [ "dat_list", "ME405-Lab0x03-PC-RHall_8py.html#a4dfa3afd05b30f277f22464478395c71", null ],
    [ "filename", "ME405-Lab0x03-PC-RHall_8py.html#a39eef220daf209403c90d847f8219ed7", null ],
    [ "min_inx", "ME405-Lab0x03-PC-RHall_8py.html#aaa9b66318cc82f442ec2e425e9165e1d", null ],
    [ "newline", "ME405-Lab0x03-PC-RHall_8py.html#a56f87feb06b9c32729cef5ac4321d2d5", null ],
    [ "point", "ME405-Lab0x03-PC-RHall_8py.html#ace71c51d4f0920c209be737e967a2166", null ],
    [ "ser", "ME405-Lab0x03-PC-RHall_8py.html#a2d2b66790e4149f65f31607ce14b3224", null ],
    [ "tau", "ME405-Lab0x03-PC-RHall_8py.html#a9b20d6951a6536b5342507bd266c99b3", null ],
    [ "tau_us", "ME405-Lab0x03-PC-RHall_8py.html#ac65479e7019ca1139c24492bcca1af5c", null ],
    [ "time", "ME405-Lab0x03-PC-RHall_8py.html#af7063b04320b141aefd60d536ac3e075", null ],
    [ "time_step", "ME405-Lab0x03-PC-RHall_8py.html#a8be61f7d4c44159413732a8e15f3a54f", null ],
    [ "val", "ME405-Lab0x03-PC-RHall_8py.html#a768b597cc50d9e35ac428775342cf4e0", null ],
    [ "volt_factor", "ME405-Lab0x03-PC-RHall_8py.html#a56e8b629a16ee6cd1a4bf61f022600b0", null ],
    [ "volts", "ME405-Lab0x03-PC-RHall_8py.html#a62ea6d9bad554b89828944b0ec8440b7", null ],
    [ "write", "ME405-Lab0x03-PC-RHall_8py.html#acad5f9058d44b5b8674450b730edefe2", null ]
];