var ME405_Lab0x04_RHall_MHughes_8py =
[
    [ "adcall", "ME405-Lab0x04-RHall-MHughes_8py.html#a4a113319e861942170d615649b51b2ef", null ],
    [ "amb_temp", "ME405-Lab0x04-RHall-MHughes_8py.html#a0d9058df92349030ae65d7735b31ea77", null ],
    [ "ambBuf", "ME405-Lab0x04-RHall-MHughes_8py.html#aade290fcf59b728a63177845513e2f94", null ],
    [ "corBuf", "ME405-Lab0x04-RHall-MHughes_8py.html#afc73f3cb2e92b1c129fd187f0486629a", null ],
    [ "core_temp", "ME405-Lab0x04-RHall-MHughes_8py.html#aba4f22892ac7a78c4eb5b86f019e4515", null ],
    [ "line", "ME405-Lab0x04-RHall-MHughes_8py.html#aa0db94292e88b21da66812d33718bf34", null ],
    [ "lpInx", "ME405-Lab0x04-RHall-MHughes_8py.html#a3de834a8447296673fb550a1f4cd9254", null ],
    [ "MCP", "ME405-Lab0x04-RHall-MHughes_8py.html#acec4531317706ebce3828e9c7141aca6", null ],
    [ "msg", "ME405-Lab0x04-RHall-MHughes_8py.html#a22a737232a35282ec59682cd60c7be15", null ],
    [ "prnt", "ME405-Lab0x04-RHall-MHughes_8py.html#a515bf590ac862a739067dc912a12536f", null ],
    [ "rnTim", "ME405-Lab0x04-RHall-MHughes_8py.html#a576cc37a878375eec339586ea1189016", null ],
    [ "rnTimS", "ME405-Lab0x04-RHall-MHughes_8py.html#af3f66e67522bc829a3544c7db375cfda", null ],
    [ "t0", "ME405-Lab0x04-RHall-MHughes_8py.html#a9315b3a37bc43d21a65b074b3490d570", null ],
    [ "t1", "ME405-Lab0x04-RHall-MHughes_8py.html#a96f30cc5cdadc1d79024ddf2dbe614dc", null ],
    [ "t_total", "ME405-Lab0x04-RHall-MHughes_8py.html#a78cb2f11cf0f101338da97fee5eae7e5", null ],
    [ "tdif", "ME405-Lab0x04-RHall-MHughes_8py.html#ac5db9b242c14289f580462b908440081", null ],
    [ "timBuf", "ME405-Lab0x04-RHall-MHughes_8py.html#a802c28f2d938b793bfdebf3d40007b6a", null ],
    [ "time", "ME405-Lab0x04-RHall-MHughes_8py.html#a896044e1344477773c42bac05aa6e1a5", null ],
    [ "time_sec", "ME405-Lab0x04-RHall-MHughes_8py.html#adec2bf33dbf10224336d91ada730af25", null ],
    [ "tmr", "ME405-Lab0x04-RHall-MHughes_8py.html#a3a65e2604d793bb79a627b0109c15aac", null ]
];