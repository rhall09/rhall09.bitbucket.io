var Lab0x04_RHall_MHughes_8py =
[
    [ "adcall", "Lab0x04-RHall-MHughes_8py.html#ac148da0213d6a5bd69cc7fbfc92de35e", null ],
    [ "amb_temp", "Lab0x04-RHall-MHughes_8py.html#aedfe0eebaa6a73973e2ea4aa15535f4c", null ],
    [ "core_temp", "Lab0x04-RHall-MHughes_8py.html#a6212ffe75ec484b86a7da3f899658226", null ],
    [ "line", "Lab0x04-RHall-MHughes_8py.html#a0c93996617fe47b52c9aa4ae75b14dae", null ],
    [ "MCP", "Lab0x04-RHall-MHughes_8py.html#ae45f17d841f4f44d52201f0695f1b973", null ],
    [ "prnt", "Lab0x04-RHall-MHughes_8py.html#abbba248e9a1a8ce3e8bff264ee0ac9f5", null ],
    [ "rnTim", "Lab0x04-RHall-MHughes_8py.html#ac61224d0b3755b9eecbabe89e66383c9", null ],
    [ "rnTimS", "Lab0x04-RHall-MHughes_8py.html#ad3ea77163d5fcd2776ae4ddc119bdde8", null ],
    [ "t0", "Lab0x04-RHall-MHughes_8py.html#abd5161477e4f30e0d52adf24ed97d425", null ],
    [ "t1", "Lab0x04-RHall-MHughes_8py.html#a90f91136b91404864d083c87e9dbd6ec", null ],
    [ "t_total", "Lab0x04-RHall-MHughes_8py.html#aeef143786f52803941379b9efc2966be", null ],
    [ "tdif", "Lab0x04-RHall-MHughes_8py.html#a8700ed426503d3f437afd5eeca545c9e", null ],
    [ "time", "Lab0x04-RHall-MHughes_8py.html#a05553f1aadbe6c614399cd4c4e18c6aa", null ],
    [ "time_sec", "Lab0x04-RHall-MHughes_8py.html#ab8c637296986aea4fa8a48b71894d2c7", null ]
];