/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME405 Mechatronics Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "References", "index.html#sec_ref", null ],
    [ "Lab-0x00 - Software Installation Verification", "page_Lab0.html", [
      [ "Lab 0x00 Evidence Submission", "page_Lab0.html#sec_lab0", null ]
    ] ],
    [ "Lab 0x01 - Vendotron Virtual Vending Machine", "page_Lab1.html", [
      [ "Vendotron Description", "page_Lab1.html#page_Lab1_desc", null ],
      [ "Vendotron User Manual", "page_Lab1.html#page_Lab1_usermanual", null ],
      [ "Vendotron Finite State Machine", "page_Lab1.html#page_Lab1_fsm", null ],
      [ "Source Code", "page_Lab1.html#page_Lab1_sourcecode", null ],
      [ "Vendotron Documentation", "page_Lab1.html#page_Lab1_documentation", null ]
    ] ],
    [ "Lab 0x02 - Nucleo Reaction Time Tester", "page_Lab2.html", [
      [ "Reaction Tester Description", "page_Lab2.html#page_Lab2_desc", null ],
      [ "Source Code", "page_Lab2.html#page_Lab2_sourcecode", null ],
      [ "Reaction Tester Documentation", "page_Lab2.html#page_Lab2_documentation", null ]
    ] ],
    [ "Lab 0x03 - Nucleo Button Step Response", "page_Lab3.html", [
      [ "Step Response Test Description", "page_Lab3.html#page_Lab3_desc", null ],
      [ "Step Response User Manual", "page_Lab3.html#page_Lab3_usermanual", null ],
      [ "Source Code", "page_Lab3.html#page_Lab3_sourcecode", null ],
      [ "Reaction Tester Documentation", "page_Lab3.html#page_Lab3_documentation", null ]
    ] ],
    [ "Lab 0x04 - Temperature Data Recorder", "page_Lab4.html", [
      [ "Temperature Recorder Description", "page_Lab4.html#page_Lab4_desc", null ],
      [ "Temperature Recorder User Manual", "page_Lab4.html#page_Lab4_usermanual", null ],
      [ "Source Code", "page_Lab4.html#page_Lab4_sourcecode", null ],
      [ "Temperature Sensor Documentation", "page_Lab4.html#page_Lab4_documentation", null ]
    ] ],
    [ "Lab 0x05 - Balancing Table Analysis Model", "page_Lab5.html", [
      [ "Analysis Part 1 Description - System Modeling", "page_Lab5.html#page_Lab5_desc", null ],
      [ "Hand Calculation Scans", "page_Lab5.html#page_Lab5_calcs", null ]
    ] ],
    [ "Lab 0x06 - Balancing Table Analysis Simulation", "page_Lab6.html", [
      [ "Analysis Part 2 Description - System Simulation", "page_Lab6.html#page_Lab6_desc", null ],
      [ "Simulation Methodology", "page_Lab6.html#page_Lab6_sim", null ],
      [ "Simulation Results", "page_Lab6.html#page_Lab6_res", null ],
      [ "Matlab Source Code", "page_Lab6.html#page_Lab6_sourcecode", null ],
      [ "Hand Calculation Scans", "page_Lab6.html#page_Lab6_calcs", null ]
    ] ],
    [ "Lab 0x07 - Balancing Table Touchplate Driver", "page_Lab7.html", [
      [ "Touchplate Driver Description", "page_Lab7.html#page_Lab7_desc", null ],
      [ "Touchplate Driver User Manual", "page_Lab7.html#page_Lab7_usermanual", null ],
      [ "Source Code", "page_Lab7.html#page_Lab7_sourcecode", null ],
      [ "touchPanel Documentation", "page_Lab7.html#page_Lab7_documentation", null ]
    ] ],
    [ "Lab 0x08 - Balancing Table Motor and Encoder Drivers", "page_Lab8.html", [
      [ "Motor and Encoder Driver Descriptions", "page_Lab8.html#page_Lab8_desc", null ],
      [ "Partnership", "page_Lab8.html#page_Lab8_partnership", null ],
      [ "Source Code", "page_Lab8.html#page_Lab8_sourcecode", null ],
      [ "Documentation", "page_Lab8.html#page_Lab8_documentation", null ]
    ] ],
    [ "Term Project - Balancing Table", "page_TermProject.html", [
      [ "Balancing Table Description", "page_TermProject.html#page_TermProject_desc", null ],
      [ "Results", "page_TermProject.html#page_TermProject_results", null ],
      [ "User Manual", "page_TermProject.html#page_TermProject_usermanual", null ],
      [ "Partnership", "page_TermProject.html#page_TermProject_partnership", null ],
      [ "References", "page_TermProject.html#page_TermProject_references", null ],
      [ "Source Code", "page_TermProject.html#page_TermProject_sourcecode", null ],
      [ "Documentation", "page_TermProject.html#page_TermProject_documentation", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"EncoderDriver_8py.html",
"MotorDriver_8py.html#a815f95d4e5514a720e253e4f92aea4f0",
"page_Lab6.html#page_Lab6_sourcecode"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';