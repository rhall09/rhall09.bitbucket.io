var ME405_Lab0x03_Board_RHall_8py =
[
    [ "btnPress", "ME405-Lab0x03-Board-RHall_8py.html#a985a8d39715d5523f81b0a375de9e5db", null ],
    [ "adc", "ME405-Lab0x03-Board-RHall_8py.html#a2a13da374de36015f19901449d2dbf06", null ],
    [ "adcBuf", "ME405-Lab0x03-Board-RHall_8py.html#a40764b92c6751b4942650d167926a07e", null ],
    [ "btn", "ME405-Lab0x03-Board-RHall_8py.html#a36be789dadf25990ebf7ac68e4e052f4", null ],
    [ "extint", "ME405-Lab0x03-Board-RHall_8py.html#a1accb7a4ab7f9565e2eb78c6c9b0ce51", null ],
    [ "keyBool", "ME405-Lab0x03-Board-RHall_8py.html#a79b47ee0ee3cad846afb3b5bcb3ce90d", null ],
    [ "preBuf", "ME405-Lab0x03-Board-RHall_8py.html#a8cb2986f15e3bc81d01066e6defbe3f2", null ],
    [ "runADC", "ME405-Lab0x03-Board-RHall_8py.html#ad1f6b89c49d86d9c0cf9c6c79e6b7d69", null ],
    [ "ser", "ME405-Lab0x03-Board-RHall_8py.html#ade275d34a3c8b8300f48bfb4b4ab201d", null ],
    [ "tim", "ME405-Lab0x03-Board-RHall_8py.html#ab2ff6b2a3f67dfae9c99410f6534f2c3", null ],
    [ "val", "ME405-Lab0x03-Board-RHall_8py.html#a62a408608694634736660786ef3fff02", null ]
];