var files_dup =
[
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", "EncoderDriver_8py" ],
    [ "MCP9808.py", "MCP9808_8py.html", "MCP9808_8py" ],
    [ "ME405-Lab0x01-RHall.py", "ME405-Lab0x01-RHall_8py.html", "ME405-Lab0x01-RHall_8py" ],
    [ "ME405-Lab0x02-RHall.py", "ME405-Lab0x02-RHall_8py.html", "ME405-Lab0x02-RHall_8py" ],
    [ "ME405-Lab0x03-Board-RHall.py", "ME405-Lab0x03-Board-RHall_8py.html", "ME405-Lab0x03-Board-RHall_8py" ],
    [ "ME405-Lab0x03-PC-RHall.py", "ME405-Lab0x03-PC-RHall_8py.html", "ME405-Lab0x03-PC-RHall_8py" ],
    [ "ME405-Lab0x04-RHall-MHughes.py", "ME405-Lab0x04-RHall-MHughes_8py.html", "ME405-Lab0x04-RHall-MHughes_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "print_task.py", "print__task_8py.html", "print__task_8py" ],
    [ "projectFile.py", "projectFile_8py.html", "projectFile_8py" ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "touchCal.py", "touchCal_8py.html", "touchCal_8py" ],
    [ "TouchPanel.py", "TouchPanel_8py.html", "TouchPanel_8py" ]
];